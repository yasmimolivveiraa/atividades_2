//Testando o operador OU - ||
namespace operador_Ou{
    let idade = 12; 
    let maiorIdade = idade > 14; 
    let possuiAutorizacaoDosPais = false; 

    let podeBeber = maiorIdade || possuiAutorizacaoDosPais; 

    console.log(podeBeber); // true


}