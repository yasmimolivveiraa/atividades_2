//Testando o operador E - &&
namespace operador_E{

    let idade = 20; 
    let maiorIdade = idade > 18; 
    let PossuiCarteiraDeMotorista = true; 

    let podeDirigir = maiorIdade && PossuiCarteiraDeMotorista;

    console.log(podeDirigir); // true
}
